import asyncio

class Command(object):
	def __init__(self, command_string):
		self.command_string = command_string.lower()

	def match(self, candidate_command_string):
		return self.command_string == candidate_command_string.lower()

	def fire(self, parts, channel, author):
		self.parts = parts
		self.channel = channel
		self.author = author
		asyncio.ensure_future(self.main())

	async def e(self, method, *args, **kwargs):
		loop = asyncio.get_event_loop()
		return await loop.run_in_executor(None, method, *args, **kwargs)

	async def main(self):
		raise NotImplemeted()
