import os, sys

path = os.path.dirname(os.path.abspath(__file__))

# import everything from command directory
for py in [f[:-3] for f in os.listdir(path) if f.endswith('.py') and f != '__init__.py']:
    mod = __import__('.'.join([__name__, py]), fromlist=[py])
    classes = [getattr(mod, x) for x in dir(mod) if isinstance(getattr(mod, x), type)]
    for cls in classes:
        setattr(sys.modules[__name__], cls.__name__, cls)

COMMANDS_AVAILABLE = list( map(
	lambda klass: klass(),
	command.Command.__subclasses__()
))

def run(parts, channel, author):
	if len(parts) == 0:
		return False

	for cmd in COMMANDS_AVAILABLE:
		if cmd.match( parts[0] ):
			cmd.fire( parts, channel, author )
			return True

	return False
